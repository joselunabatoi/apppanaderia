package com.example.apppanaderia.ui.main;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.apppanaderia.Fragments.Dulce;
import com.example.apppanaderia.Fragments.Embolsados;
import com.example.apppanaderia.Fragments.Magdalenas;
import com.example.apppanaderia.Fragments.Panes;
import com.example.apppanaderia.Fragments.Reposteria;
import com.example.apppanaderia.Fragments.Salado;
import com.example.apppanaderia.Fragments.Todos;
import com.example.apppanaderia.Fragments.Tonyas;
import com.example.apppanaderia.Fragments.verPedidosFragment;
import com.example.apppanaderia.R;

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {

    @StringRes
    private static final int[] TAB_TITLES = new int[]{R.string.tab_text_1, R.string.tab_text_2,R.string.tab_text_3,R.string.tab_text_4,R.string.tab_text_5,R.string.tab_text_6,R.string.tab_text_7,R.string.tab_text_8,R.string.tab_text_9};
    private final Context mContext;

    public SectionsPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
//        return PlaceholderFragment.newInstance(position + 1);

        switch (position){
            case 0:
                Fragment fragment1 = new Todos();
                return fragment1;
            case 1:
                Fragment fragment2 = new Embolsados();
                return fragment2;
            case 2:
                Fragment fragment3 = new Panes();
                return fragment3;
            case 3:
                Fragment fragment4 = new Tonyas();
                return fragment4;
            case 4:
                Fragment fragment5 = new Magdalenas();
                return fragment5;
            case 5:
                Fragment fragment6 = new Dulce();
                return fragment6;
            case 6:
                Fragment fragment7 = new Salado();
                return fragment7;
            case 7:
                Fragment fragment8 = new Reposteria();
                return fragment8;
            case 8:
                Fragment fragment9 = new verPedidosFragment();
                return fragment9;
        }
        return null;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getResources().getString(TAB_TITLES[position]);
    }

    @Override
    public int getCount() {
        // Show 2 total pages.
        return 9;
    }
}