package com.example.apppanaderia.Objetos;

public class Pedir {
    private int IDprodcto;
    private int IDpedido;
    private int cantidad;

    public Pedir(int IDprodcto, int IDpedido, int cantidad) {
        this.IDprodcto = IDprodcto;
        this.IDpedido = IDpedido;
        this.cantidad = cantidad;
    }

    public Pedir() {
    }

    public int getIDprodcto() {
        return IDprodcto;
    }

    public void setIDprodcto(int IDprodcto) {
        this.IDprodcto = IDprodcto;
    }

    public int getIDpedido() {
        return IDpedido;
    }

    public void setIDpedido(int IDpedido) {
        this.IDpedido = IDpedido;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
}
