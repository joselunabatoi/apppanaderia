package com.example.apppanaderia;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Toast;

import com.example.apppanaderia.Adapters.AdapterElegir;
import com.example.apppanaderia.Adapters.AdapterPedidos;
import com.example.apppanaderia.Adapters.AdapterTodos;
import com.example.apppanaderia.DAO.ProductosDAO;
import com.example.apppanaderia.Objetos.Pedir;
import com.example.apppanaderia.Objetos.Productos;

import java.util.ArrayList;
import java.util.List;

public class ElegirProductos extends AppCompatActivity {
    private RecyclerView recycler;
    private AdapterElegir adapProductos;
    public static ArrayList<Pedir> productospedidos;
    public static ArrayList<Productos> listaProductos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_elegir_productos);
        setUI();
    }

    private void setUI() {

        recycler = findViewById(R.id.RecyclerProductos);
        recycler.setLayoutManager(new LinearLayoutManager(this));

        productospedidos = new ArrayList<>();
        listaProductos = new ArrayList<>();

        adapProductos = new AdapterElegir(obtenerPedidos(),getApplicationContext());
        recycler.setAdapter(adapProductos);

    }

    private ArrayList<Productos> obtenerPedidos() {
        ArrayList<Productos> productosista = new ArrayList<>();

        productosista.add(new Productos(1,"Barra casera",1,"panes","barra_casera"));
        productosista.add(new Productos(2,"Pan de cereales",1.5f,"panes","pan_cereales"));
        productosista.add(new Productos(3,"Coca de harina",2,"salado","coca_harina"));
        productosista.add(new Productos(4,"Coca de pizza",2.5f,"salado","coca_pizza"));
        productosista.add(new Productos(5,"Magdalenas bombon",3,"magdalenas","magdalenas_bombon"));
        productosista.add(new Productos(6,"Magdalenas caseras",3.5f,"magdalenas","magdalenas_casera"));
        productosista.add(new Productos(7,"Mantecado de almendra",4,"reposteria","mantecado_almendra"));
        productosista.add(new Productos(8,"Mantecado",4.5f,"reposteria","mantecados"));
        productosista.add(new Productos(9,"Rollo",5,"tonyas","rollos"));
        productosista.add(new Productos(10,"Toña",5.5f,"tonyas","tonya"));
        productosista.add(new Productos(11,"Sequillos en bolsa",6,"embolsado","sequillos_bolsa"));
        productosista.add(new Productos(12,"Sequillos sin azucar en bolsa",6.5f,"embolsado","sequillos_sin_azucar_bolsa"));


//        ProductosDAO proDAO;
//        try {
//            proDAO = new ProductosDAO();
//            productosista.addAll(proDAO.findAll());
//
//        }catch (Exception e){
//            System.out.println(e.getMessage());
//        }

        return productosista;
    }
}