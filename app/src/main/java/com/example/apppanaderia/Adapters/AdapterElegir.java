package com.example.apppanaderia.Adapters;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.apppanaderia.ElegirProductos;
import com.example.apppanaderia.Objetos.Pedir;
import com.example.apppanaderia.Objetos.Productos;
import com.example.apppanaderia.R;

import java.util.ArrayList;

public class AdapterElegir extends RecyclerView.Adapter<AdapterElegir.MyViewHolder> {
    private ArrayList<Productos> productos;
    private Context context;


    static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvPrecio, tvDescripcion, tvTitPrecio, tvTitDesc;
        ImageView imagen;
        Context cont;



        public MyViewHolder(View view, ArrayList<Productos> productos, Context cont) {
            super(view);
            tvPrecio = view.findViewById(R.id.tvPrecioTodos);
            tvDescripcion = view.findViewById(R.id.tvDescTodos);
            imagen = view.findViewById(R.id.ImagenTodos);
            tvTitDesc = view.findViewById(R.id.tvTitDescripcion);
            tvTitPrecio = view.findViewById(R.id.tvTitPrecio);
            this.cont = cont;


        }

        public void bind(Productos productos) {
            if (productos.getImagen().equals("sequillos_bolsa")) {
                this.imagen.setImageResource(R.drawable.sequillos_bolsa);
            } else if (productos.getImagen().equals("sequillos_sin_azucar_bolsa")) {
                this.imagen.setImageResource(R.drawable.sequillos_sin_azucar_bolsa);
            } else if (productos.getImagen().equals("barra_casera")) {
                this.imagen.setImageResource(R.drawable.barra_casera);
            } else if (productos.getImagen().equals("coca_harina")) {
                this.imagen.setImageResource(R.drawable.coca_harina);
            } else if (productos.getImagen().equals("coca_pizza")) {
                this.imagen.setImageResource(R.drawable.coca_pizza);
            } else if (productos.getImagen().equals("magdalenas_bombon")) {
                this.imagen.setImageResource(R.drawable.magdalenas_bombon);
            } else if (productos.getImagen().equals("magdalenas_casera")) {
                this.imagen.setImageResource(R.drawable.magdalenas_casera);
            } else if (productos.getImagen().equals("mantecado_almendra")) {
                this.imagen.setImageResource(R.drawable.mantecado_almendra);
            } else if (productos.getImagen().equals("mantecados")) {
                this.imagen.setImageResource(R.drawable.mantecados);
            } else if (productos.getImagen().equals("pan_cereales")) {
                this.imagen.setImageResource(R.drawable.pan_cereales);
            } else if (productos.getImagen().equals("rollos")) {
                this.imagen.setImageResource(R.drawable.rollos);
            } else if (productos.getImagen().equals("tonya")) {
                this.imagen.setImageResource(R.drawable.tonya);
            }
            this.tvDescripcion.setText(productos.getDescripcion());
            this.tvTitPrecio.setText("Precio:");
            this.tvTitDesc.setText("Producto:");
            String preco = String.valueOf(productos.getPrecio());
            this.tvPrecio.setText(preco + "€");

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Pedir pedir = new Pedir();
                    pedir.setIDprodcto(productos.getID());
                    pedir.setIDpedido(AdapterPedidos.Contador);
                    Toast.makeText(cont.getApplicationContext(), "Producto añadido",Toast.LENGTH_LONG).show();
//                    Toast.makeText(cont.getApplicationContext(), "producto:"+productos.getID()+" pedido:"+AdapterPedidos.Contador,Toast.LENGTH_LONG).show();
//                    ElegirProductos.productospedidos.add(pedir);

//                    pedir.setCantidad();
                }
            });
        }
    }

    public AdapterElegir(ArrayList<Productos> productos, Context context) {
        this.productos = productos;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_todos, parent, false);
        return new MyViewHolder(itemLayout, productos, context);

    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.bind(productos.get(position));
    }

    @Override
    public int getItemCount() {
        return productos.size();
    }
}
