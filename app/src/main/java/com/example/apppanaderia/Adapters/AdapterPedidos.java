package com.example.apppanaderia.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.apppanaderia.Objetos.Pedidos;
import com.example.apppanaderia.R;

import java.util.ArrayList;

public class AdapterPedidos extends RecyclerView.Adapter<AdapterPedidos.MyViewHoldier> {
    private ArrayList<Pedidos> pedidos;
    private Context context;
    public static int Contador = 0;

    public class MyViewHoldier extends RecyclerView.ViewHolder {
        TextView tvNumPedido, tvFechaPedido, tvFechaEntregado;
        ImageView imagenEntregado;
        Context cont;

        public MyViewHoldier(@NonNull View view, ArrayList<Pedidos> pedidos, Context cont) {
            super(view);
            tvNumPedido = view.findViewById(R.id.tvNumPedido);
            tvFechaPedido = view.findViewById(R.id.tvFechaPedido);
            tvFechaEntregado = view.findViewById(R.id.tvFechaEntregado);
            imagenEntregado = view.findViewById(R.id.imagenEntregado);
            this.cont = cont;
        }

        public void bind(Pedidos pedidos){
            if (pedidos.isEntregado()){
                this.imagenEntregado.setImageResource(R.drawable.check);
            }else{
                this.imagenEntregado.setImageResource(R.drawable.cancel);
            }

            this.tvNumPedido.setText(String.valueOf(Contador));
            sumarNumero();
            this.tvFechaPedido.setText(pedidos.getFechapedido());
            this.tvFechaEntregado.setText(pedidos.getFechaentrega());


        }
    }

    public AdapterPedidos(ArrayList<Pedidos> pedidos, Context context) {
        this.pedidos = pedidos;
        this.context = context;
    }

    public void sumarNumero(){
        Contador++;
    }

    @NonNull
    @Override
    public AdapterPedidos.MyViewHoldier onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_pedidos,parent,false);
        return new MyViewHoldier(itemLayout,pedidos,context);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterPedidos.MyViewHoldier holder, int position) {
        holder.bind (pedidos.get(position));
    }

    @Override
    public int getItemCount() {
        return pedidos.size();
    }


}
