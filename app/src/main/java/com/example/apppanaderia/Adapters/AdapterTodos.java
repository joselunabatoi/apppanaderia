package com.example.apppanaderia.Adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.apppanaderia.Objetos.Productos;
import com.example.apppanaderia.R;

import java.util.ArrayList;

public class AdapterTodos extends RecyclerView.Adapter<AdapterTodos.MyViewHolder> {
    private ArrayList<Productos> productos;
    private Context context;

    static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvPrecio, tvDescripcion, tvTitPrecio, tvTitDesc;
        ImageView imagen;
        Context cont;

        public MyViewHolder(View view, ArrayList<Productos> productos, Context cont){
            super(view);
            tvPrecio = view.findViewById(R.id.tvPrecioTodos);
            tvDescripcion = view.findViewById(R.id.tvDescTodos);
            imagen = view.findViewById(R.id.ImagenTodos);
            tvTitDesc = view.findViewById(R.id.tvTitDescripcion);
            tvTitPrecio = view.findViewById(R.id.tvTitPrecio);
            this.cont = cont;
        }

        public void bind(Productos productos) {
            if (productos.getImagen().equals("sequillos_bolsa")){
                this.imagen.setImageResource(R.drawable.sequillos_bolsa);
            }else if (productos.getImagen().equals("sequillos_sin_azucar_bolsa")){
                this.imagen.setImageResource(R.drawable.sequillos_sin_azucar_bolsa);
            }else if (productos.getImagen().equals("barra_casera")){
                this.imagen.setImageResource(R.drawable.barra_casera);
            }else if (productos.getImagen().equals("coca_harina")){
                this.imagen.setImageResource(R.drawable.coca_harina);
            }else if (productos.getImagen().equals("coca_pizza")){
                this.imagen.setImageResource(R.drawable.coca_pizza);
            }else if (productos.getImagen().equals("magdalenas_bombon")){
                this.imagen.setImageResource(R.drawable.magdalenas_bombon);
            }else if (productos.getImagen().equals("magdalenas_casera")){
                this.imagen.setImageResource(R.drawable.magdalenas_casera);
            }else if (productos.getImagen().equals("mantecado_almendra")){
                this.imagen.setImageResource(R.drawable.mantecado_almendra);
            }else if (productos.getImagen().equals("mantecados")){
                this.imagen.setImageResource(R.drawable.mantecados);
            }else if (productos.getImagen().equals("pan_cereales")){
                this.imagen.setImageResource(R.drawable.pan_cereales);
            }else if (productos.getImagen().equals("rollos")){
                this.imagen.setImageResource(R.drawable.rollos);
            }else if (productos.getImagen().equals("tonya")){
                this.imagen.setImageResource(R.drawable.tonya);
            }
            this.tvDescripcion.setText(productos.getDescripcion());
            this.tvTitPrecio.setText("Precio:");
            this.tvTitDesc.setText("Producto:");
            String preco = String.valueOf(productos.getPrecio());
            this.tvPrecio.setText(preco+"€");
        }
    }

    public AdapterTodos(ArrayList<Productos> productos, Context context){
        this.productos = productos;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_todos, parent, false);
            return new MyViewHolder(itemLayout, productos, context);

    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.bind (productos.get(position));
    }

    @Override
    public int getItemCount() {
        return productos.size();
    }

}
