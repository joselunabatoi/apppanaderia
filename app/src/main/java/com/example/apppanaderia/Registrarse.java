package com.example.apppanaderia;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.apppanaderia.DAO.UsuariosDAO;
import com.example.apppanaderia.Objetos.Usuarios;

import java.util.ArrayList;

public class Registrarse extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrarse);
        setUI();
    }

    private void setUI() {
        Button btCancelar,btRegistrar;
        EditText etNombre, etContrasenya, etApellidos;

        btCancelar = findViewById(R.id.btCancelarRegUsuario);
        btRegistrar = findViewById(R.id.btRegistrarUsuario);
        etNombre = findViewById(R.id.etRegNombre);
        etContrasenya = findViewById(R.id.etRegContra);
        etApellidos = findViewById(R.id.etRegApellidos);

        btCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                Intent intent = new Intent(getApplicationContext(), Login.class);
                startActivity(intent);

//                UsuariosDAO userDao;
//                try{
//                userDao = new UsuariosDAO();
//                Usuarios user = new Usuarios();
//                user.setNombre(etNombre.getText().toString());
//                user.setContraseña(etContrasenya.getText().toString());
//                user.setApelldos(etApellidos.getText().toString());
//                Boolean insertado = userDao.insert(user);
//
//                if (insertado){
//                    Intent intent = new Intent(getApplicationContext(), Login.class);
//                    startActivity(intent);
//                }else{
//                    Toast.makeText(getApplicationContext(),"No se ha podido crear el usuario",Toast.LENGTH_LONG).show();
//                }
//                }catch (Exception e){
//                  System.out.println(e.getMessage());
//                }
            }
        });

        btRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Usuarios user = new Usuarios();
                user.setContraseña(etContrasenya.getText().toString());
                user.setNombre(etNombre.getText().toString());
                user.setApelldos(etApellidos.getText().toString());

                ArrayList<Usuarios> usuarios = new ArrayList<>();

                if (!etNombre.getText().toString().equals("")){
                    if (!etContrasenya.getText().toString().equals("")){
                        if (!etApellidos.getText().toString().equals("")){

                            if (usuarios.isEmpty()){
                                Login.usuariosPermitidos.add(user);
                                Toast.makeText(getApplicationContext(),"Usuario Registrado",Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(getApplicationContext(), Login.class);
                                startActivity(intent);
                            }else{
                                for (int i = 0; i < Login.usuariosPermitidos.size(); i++) {
                                    if (user.getNombre().equals(Login.usuariosPermitidos.get(i).getNombre()) && user.getContraseña().equals(Login.usuariosPermitidos.get(i).getContraseña()) && user.getApelldos().equals(Login.usuariosPermitidos.get(i).getApelldos())){
                                        usuarios.add(Login.usuariosPermitidos.get(i));

                                    }
                                }
                            }



                        }else{
                            Toast.makeText(getApplicationContext(),"Porfavor Introduzca los apellidos",Toast.LENGTH_LONG).show();
                        }
                    }else{
                        Toast.makeText(getApplicationContext(),"Porfavor Introduzca la contraseña",Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(getApplicationContext(),"Porfavor Introduzca el nombre",Toast.LENGTH_LONG).show();
                }
            }
        });



    }
}