package com.example.apppanaderia;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TextView;

import com.example.apppanaderia.Objetos.Productos;

import java.util.ArrayList;

public class CrearPedido extends AppCompatActivity {
    public static ArrayList<Productos> listaProductos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_pedido);
        setUI();
    }

    private void setUI() {
        EditText etTelefono, etNombre;
        Button btElegir, btCancelarPed, btCrear;
        DatePicker datepicker;
        TableLayout tablapedidos;
        TextView tvTotal;

        listaProductos = new ArrayList<>();

        etTelefono = findViewById(R.id.etTelefonoPedido);
        etNombre = findViewById(R.id.etNombrePedido);
        btElegir = findViewById(R.id.btElegirProductos);
        btCrear = findViewById(R.id.btCrearPedido);
        datepicker = findViewById(R.id.DatePickerPedido);
        tablapedidos = findViewById(R.id.TablaProductos);
        tvTotal = findViewById(R.id.tvTotalPedido);
        btCancelarPed = findViewById(R.id.btCancelarPedido);

        btCancelarPed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btElegir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),ElegirProductos.class);
                startActivity(intent);
            }
        });







    }

}