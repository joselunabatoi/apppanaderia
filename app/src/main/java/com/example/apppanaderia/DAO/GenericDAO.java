package com.example.apppanaderia.DAO;

import java.util.List;

public interface GenericDAO<Tipo>{
    Tipo findByPK(int id) throws Exception;

    List<Tipo> findAll() throws Exception;

    boolean insert(Tipo t) throws Exception;
    Tipo insertGenKey(Tipo t) throws Exception;

    boolean update(Tipo t) throws Exception;

    boolean delete(int id) throws Exception;
    boolean delete(Tipo t) throws Exception;

    int size() throws Exception;
    boolean exists(int id) throws Exception;
    List<Tipo> findByExample(Tipo t) throws Exception;

}
