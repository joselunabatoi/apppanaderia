package com.example.apppanaderia.DAO;

import com.example.apppanaderia.Objetos.Pedidos;
import com.example.apppanaderia.Objetos.Productos;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class PedidosDAO implements GenericDAO<Pedidos>{
    final String SQLSELECTALL = "SELECT * FROM pedidos";
    final String SQLSELECTCOUNT = "SELECT count(*) FROM pedidos";
    final String SQLSELECTPK = "SELECT * FROM pedidos WHERE id = ?";
    final String SQLSELECTEXIST = "SELECT * from pedidos where id = ?";
    final String SQLINSERT = "INSERT INTO pedidos (telefono, fecha_pedido, fecha_entrega, nombre,IDusuario,entregado) VALUES (?, ?,?,?,?,?)";
    final String SQLUPDATE = "UPDATE pedidos SET telefono = ?, fecha_pedido = ?,fecha_entrega = ?, nombre = ?, IDusuario = ?, entregado = ? WHERE id = ?";
    final String SQLDELETE = "DELETE FROM pedidos WHERE id = ?";
    private final PreparedStatement pstSelectPK;
    private final PreparedStatement pstSelectCount;
    private final PreparedStatement pstSelectExist;
    private final PreparedStatement pstSelectAll;
    private final PreparedStatement pstInsert;
    private final PreparedStatement pstUpdate;
    private final PreparedStatement pstDelete;

    public PedidosDAO() throws SQLException {
        Connection con = ConexionBD.getConexion();
        pstSelectPK = con.prepareStatement(SQLSELECTPK);
        pstSelectCount = con.prepareStatement(SQLSELECTCOUNT);
        pstSelectExist = con.prepareStatement(SQLSELECTEXIST);
        pstSelectAll = con.prepareStatement(SQLSELECTALL);
        pstInsert = con.prepareStatement(SQLINSERT, Statement.RETURN_GENERATED_KEYS);
        pstUpdate = con.prepareStatement(SQLUPDATE);
        pstDelete = con.prepareStatement(SQLDELETE);
    }


    public void cerrar() throws SQLException {
        pstSelectPK.close();
        pstSelectAll.close();
        pstSelectExist.close();
        pstSelectCount.close();
        pstInsert.close();
        pstUpdate.close();
        pstDelete.close();
    }

    public Pedidos findByPK(int id) throws SQLException {
        Pedidos c = null;
        pstSelectPK.setInt(1, id);
        ResultSet rs = pstSelectPK.executeQuery();
        if (rs.next()) {
            c = new Pedidos(id,rs.getInt("telefono"),rs.getString("fecha_pedido"),rs.getString("fecha_entrega"), rs.getString("nombre"), rs.getInt("IDusuario"), rs.getBoolean("entregado"));
        }
        rs.close();
        return c;
    }

    public List<Pedidos> findAll() throws SQLException {
        List<Pedidos> listaPedidos = new ArrayList<Pedidos>();
        ResultSet rs = pstSelectAll.executeQuery();
        while (rs.next()) {
            listaPedidos.add(new Pedidos(rs.getInt("ID"),rs.getInt("telefono"),rs.getString("fecha_pedido"),rs.getString("fecha_entrega"), rs.getString("nombre"), rs.getInt("IDusuario"), rs.getBoolean("entregado")));
        }
        rs.close();
        return listaPedidos;
    }

    public boolean insert(Pedidos pedInsertar) throws SQLException {
        pstInsert.setInt(1, pedInsertar.getTelefono());
        pstInsert.setString(2, pedInsertar.getFechapedido());
        pstInsert.setString(3, pedInsertar.getFechaentrega());
        pstInsert.setString(4,pedInsertar.getNombre());
        pstInsert.setInt(5,pedInsertar.getIDusuario());
        pstInsert.setBoolean(6,pedInsertar.isEntregado());
        int insertados = pstInsert.executeUpdate();
        return (insertados == 1);
    }

    @Override
    public Pedidos insertGenKey(Pedidos t) throws SQLException {
        pstInsert.setInt(1, t.getTelefono());
        pstInsert.setString(2, t.getFechapedido());
        pstInsert.setString(3, t.getFechaentrega());
        pstInsert.setString(4,t.getNombre());
        pstInsert.setInt(5,t.getIDusuario());
        pstInsert.setBoolean(6,t.isEntregado());
        int id = pstInsert.executeUpdate();
        return new Pedidos(id,t.getTelefono(),t.getFechapedido(),t.getFechaentrega(),t.getNombre(),t.getIDusuario(),t.isEntregado());
    }

    public boolean update(Pedidos pedUpdate) throws SQLException {
        pstUpdate.setInt(1, pedUpdate.getTelefono());
        pstUpdate.setString(2, pedUpdate.getFechapedido());
        pstUpdate.setString(3, pedUpdate.getFechaentrega());
        pstUpdate.setString(4,pedUpdate.getNombre());
        pstUpdate.setInt(5,pedUpdate.getIDusuario());
        pstUpdate.setBoolean(6,pedUpdate.isEntregado());
        pstUpdate.setInt(7,pedUpdate.getID());
        int actualizados = pstUpdate.executeUpdate();
        return (actualizados == 1);
    }

    public boolean delete(int id) throws SQLException {
        pstDelete.setInt(1, id);
        int borrados = pstDelete.executeUpdate();
        return (borrados == 1);
    }

    public boolean delete(Pedidos proEliminar) throws SQLException {
        return this.delete(proEliminar.getID());
    }

    @Override
    public int size() throws SQLException {
        ResultSet rs = pstSelectCount.executeQuery();
        rs.next();
        return rs.getInt(1);
    }

    @Override
    public boolean exists(int id) throws SQLException{
        pstSelectExist.setInt(1,id);
        ResultSet rs = pstSelectExist.executeQuery();
        if (rs.next()){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public List<Pedidos> findByExample(Pedidos t) throws SQLException {
        List<Pedidos> listaPedidos = new ArrayList<>();
        String sql = "Select * from pedidos where true";
        if (t.getTelefono() > 99999999){
            sql += " and telefono = "+t.getTelefono();
        }
        if (t.getFechapedido() != null){
            sql += " and fecha_pedido like '%"+t.getFechapedido()+"%'";
        }
        if (t.getFechaentrega() != null){
            sql += " and fecha_entrega like '%"+t.getFechaentrega()+"%'";
        }
        if (t.getNombre()!=null){
            sql+= " and nombre like '%"+t.getNombre()+"%'";
        }
        if (t.getIDusuario()>0){
            sql+= " and IDusuario = "+t.getNombre();
        }
        try {
            Statement st = ConexionBD.getConexion().createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                listaPedidos.add(new Pedidos(rs.getInt("ID"),rs.getInt("telefono"), rs.getString("fecha_pedido"), rs.getString("fecha_entrega"),rs.getString("nombre"),rs.getInt("IDusuario"),rs.getBoolean("entregado")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listaPedidos;
    }
}
