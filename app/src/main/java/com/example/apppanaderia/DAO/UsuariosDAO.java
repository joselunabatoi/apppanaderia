package com.example.apppanaderia.DAO;

import com.example.apppanaderia.Objetos.Usuarios;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

public class UsuariosDAO implements GenericDAO<Usuarios> {
    final String SQLSELECTALL = "SELECT * FROM usuarios";
    final String SQLSELECTCOUNT = "SELECT count(*) FROM usuarios";
    final String SQLSELECTPK = "SELECT * FROM usuarios WHERE id = ?";
    final String SQLSELECTEXIST = "SELECT * from usuarios where id = ?";
    final String SQLINSERT = "INSERT INTO usuarios (cotraseña, nombre,apellidos) VALUES (?, ?,?)";
    final String SQLUPDATE = "UPDATE usuarios SET cotraseña = ?, nombre = ?,apellidos = ? WHERE id = ?";
    final String SQLDELETE = "DELETE FROM usuarios WHERE id = ?";
    private final PreparedStatement pstSelectPK;
    private final PreparedStatement pstSelectCount;
    private final PreparedStatement pstSelectExist;
    private final PreparedStatement pstSelectAll;
    private final PreparedStatement pstInsert;
    private final PreparedStatement pstUpdate;
    private final PreparedStatement pstDelete;

    public UsuariosDAO() throws SQLException {
        Connection con = ConexionBD.getConexion();
        pstSelectPK = con.prepareStatement(SQLSELECTPK);
        pstSelectCount = con.prepareStatement(SQLSELECTCOUNT);
        pstSelectExist = con.prepareStatement(SQLSELECTEXIST);
        pstSelectAll = con.prepareStatement(SQLSELECTALL);
        pstInsert = con.prepareStatement(SQLINSERT, Statement.RETURN_GENERATED_KEYS);
        pstUpdate = con.prepareStatement(SQLUPDATE);
        pstDelete = con.prepareStatement(SQLDELETE);
    }


    public void cerrar() throws SQLException {
        pstSelectPK.close();
        pstSelectAll.close();
        pstSelectExist.close();
        pstSelectCount.close();
        pstInsert.close();
        pstUpdate.close();
        pstDelete.close();
    }

    public Usuarios findByPK(int id) throws SQLException {
        Usuarios c = null;
        pstSelectPK.setInt(1, id);
        ResultSet rs = pstSelectPK.executeQuery();
        if (rs.next()) {
            c = new Usuarios(id,rs.getString("contraseña"),rs.getString("nombre"),rs.getString("apellidos"));
        }
        rs.close();
        return c;
    }

    public List<Usuarios> findAll() throws SQLException {
        List<Usuarios> listaUsuarios = new ArrayList<Usuarios>();
        ResultSet rs = pstSelectAll.executeQuery();
        while (rs.next()) {
            listaUsuarios.add(new Usuarios(rs.getInt("ID"),rs.getString("contraseña"),rs.getString("nombre"),rs.getString("apellidos")));
        }
        rs.close();
        return listaUsuarios;
    }

    public boolean insert(Usuarios usuInsertar) throws SQLException {
        pstInsert.setString(1, usuInsertar.getContraseña());
        pstInsert.setString(2, usuInsertar.getNombre());
        pstInsert.setString(3,usuInsertar.getApelldos());
        int insertados = pstInsert.executeUpdate();
        return (insertados == 1);
    }

    @Override
    public Usuarios insertGenKey(Usuarios t) throws SQLException {
        pstInsert.setString(1, t.getContraseña());
        pstInsert.setString(2, t.getNombre());
        pstInsert.setString(3,t.getApelldos());
        int id = pstInsert.executeUpdate();
        return new Usuarios(id,t.getContraseña(),t.getNombre(),t.getApelldos());
    }

    public boolean update(Usuarios usuUpdate) throws SQLException {
        pstUpdate.setString(1, usuUpdate.getContraseña());
        pstUpdate.setString(2, usuUpdate.getNombre());
        pstUpdate.setString(3,usuUpdate.getApelldos());
        pstUpdate.setInt(4,usuUpdate.getID());
        int actualizados = pstUpdate.executeUpdate();
        return (actualizados == 1);
    }

    public boolean delete(int id) throws SQLException {
        pstDelete.setInt(1, id);
        int borrados = pstDelete.executeUpdate();
        return (borrados == 1);
    }

    public boolean delete(Usuarios usuEliminar) throws SQLException {
        return this.delete(usuEliminar.getID());
    }

    @Override
    public int size() throws SQLException {
        ResultSet rs = pstSelectCount.executeQuery();
        rs.next();
        return rs.getInt(1);
    }

    @Override
    public boolean exists(int id) throws SQLException{
        pstSelectExist.setInt(1,id);
        ResultSet rs = pstSelectExist.executeQuery();
        if (rs.next()){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public List<Usuarios> findByExample(Usuarios t) throws SQLException {
        List<Usuarios> listaUsuarios = new ArrayList<>();
        String sql = "Select * from usuarios where true";
        if (t.getContraseña() != null){
            sql += " and contraseña like '%"+t.getContraseña()+"%'";
        }
        if (t.getNombre() != null){
            sql += " and nombre like '%"+t.getNombre()+"%'";
        }
        if (t.getApelldos()!=null) {
            sql+= " and apellidos like '%"+t.getApelldos()+"%'";
        }
        try {
            Statement st = ConexionBD.getConexion().createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                listaUsuarios.add(new Usuarios(rs.getInt("ID"), rs.getString("contraseña"), rs.getString("nombre"),rs.getString("apellidos")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listaUsuarios;
    }

}
