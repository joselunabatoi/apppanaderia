package com.example.apppanaderia.DAO;

import com.example.apppanaderia.Objetos.Productos;
import com.example.apppanaderia.Objetos.Usuarios;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ProductosDAO implements GenericDAO<Productos> {
    final String SQLSELECTALL = "SELECT * FROM productos";
    final String SQLSELECTCOUNT = "SELECT count(*) FROM productos";
    final String SQLSELECTPK = "SELECT * FROM productos WHERE id = ?";
    final String SQLSELECTEXIST = "SELECT * from productos where id = ?";
    final String SQLINSERT = "INSERT INTO productos (descripcion, precio,categoria, imagen) VALUES (?, ?,?,?)";
    final String SQLUPDATE = "UPDATE productos SET descripcion = ?, precio = ?,categoria = ?, imagen = ? WHERE id = ?";
    final String SQLDELETE = "DELETE FROM productos WHERE id = ?";
    private final PreparedStatement pstSelectPK;
    private final PreparedStatement pstSelectCount;
    private final PreparedStatement pstSelectExist;
    private final PreparedStatement pstSelectAll;
    private final PreparedStatement pstInsert;
    private final PreparedStatement pstUpdate;
    private final PreparedStatement pstDelete;

    public ProductosDAO() throws SQLException {
        Connection con = ConexionBD.getConexion();
        pstSelectPK = con.prepareStatement(SQLSELECTPK);
        pstSelectCount = con.prepareStatement(SQLSELECTCOUNT);
        pstSelectExist = con.prepareStatement(SQLSELECTEXIST);
        pstSelectAll = con.prepareStatement(SQLSELECTALL);
        pstInsert = con.prepareStatement(SQLINSERT, Statement.RETURN_GENERATED_KEYS);
        pstUpdate = con.prepareStatement(SQLUPDATE);
        pstDelete = con.prepareStatement(SQLDELETE);
    }


    public void cerrar() throws SQLException {
        pstSelectPK.close();
        pstSelectAll.close();
        pstSelectExist.close();
        pstSelectCount.close();
        pstInsert.close();
        pstUpdate.close();
        pstDelete.close();
    }

    public Productos findByPK(int id) throws SQLException {
        Productos c = null;
        pstSelectPK.setInt(1, id);
        ResultSet rs = pstSelectPK.executeQuery();
        if (rs.next()) {
            c = new Productos(id,rs.getString("descripcion"),rs.getFloat("precio"),rs.getString("categoria"), rs.getString("imagen"));
        }
        rs.close();
        return c;
    }

    public List<Productos> findAll() throws SQLException {
        List<Productos> listaProductos = new ArrayList<Productos>();
        ResultSet rs = pstSelectAll.executeQuery();
        while (rs.next()) {
            listaProductos.add(new Productos(rs.getInt("ID"),rs.getString("descripcion"),rs.getFloat("precio"),rs.getString("categoria"), rs.getString("imagen")));
        }
        rs.close();
        return listaProductos;
    }

    public boolean insert(Productos proInsertar) throws SQLException {
        pstInsert.setString(1, proInsertar.getDescripcion());
        pstInsert.setFloat(2, proInsertar.getPrecio());
        pstInsert.setString(3,proInsertar.getCategoria());
        pstInsert.setString(4,proInsertar.getImagen());
        int insertados = pstInsert.executeUpdate();
        return (insertados == 1);
    }

    @Override
    public Productos insertGenKey(Productos t) throws SQLException {
        pstInsert.setString(1, t.getDescripcion());
        pstInsert.setFloat(2, t.getPrecio());
        pstInsert.setString(3,t.getCategoria());
        pstInsert.setString(4,t.getImagen());
        int id = pstInsert.executeUpdate();
        return new Productos(id,t.getDescripcion(),t.getPrecio(),t.getCategoria(),t.getImagen());
    }

    public boolean update(Productos proUpdate) throws SQLException {
        pstUpdate.setString(1, proUpdate.getDescripcion());
        pstUpdate.setFloat(2, proUpdate.getPrecio());
        pstUpdate.setString(3,proUpdate.getCategoria());
        pstUpdate.setString(4,proUpdate.getImagen());
        pstUpdate.setInt(5,proUpdate.getID());
        int actualizados = pstUpdate.executeUpdate();
        return (actualizados == 1);
    }

    public boolean delete(int id) throws SQLException {
        pstDelete.setInt(1, id);
        int borrados = pstDelete.executeUpdate();
        return (borrados == 1);
    }

    public boolean delete(Productos proEliminar) throws SQLException {
        return this.delete(proEliminar.getID());
    }

    @Override
    public int size() throws SQLException {
        ResultSet rs = pstSelectCount.executeQuery();
        rs.next();
        return rs.getInt(1);
    }

    @Override
    public boolean exists(int id) throws SQLException{
        pstSelectExist.setInt(1,id);
        ResultSet rs = pstSelectExist.executeQuery();
        if (rs.next()){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public List<Productos> findByExample(Productos t) throws SQLException {
        List<Productos> listaProductos = new ArrayList<>();
        String sql = "Select * from productos where true";
        if (t.getDescripcion() != null){
            sql += " and descripcion like '%"+t.getDescripcion()+"%'";
        }
        if (t.getPrecio() > 0){
            sql += " and precio <= "+t.getPrecio();
        }
        if (t.getCategoria()!=null) {
            sql+= " and categoria like '%"+t.getDescripcion()+"%'";
        }
        if (t.getImagen()!=null){
            sql+= " and imagen like '%"+t.getCategoria()+"%'";
        }
        try {
            Statement st = ConexionBD.getConexion().createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                listaProductos.add(new Productos(rs.getInt("ID"), rs.getString("descripcion"), rs.getFloat("precio"),rs.getString("categoria"),rs.getString("imagen")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listaProductos;
    }


}
