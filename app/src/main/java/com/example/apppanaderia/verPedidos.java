package com.example.apppanaderia;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.apppanaderia.Adapters.AdapterPedidos;
import com.example.apppanaderia.Adapters.AdapterTodos;
import com.example.apppanaderia.DAO.PedidosDAO;
import com.example.apppanaderia.Fragments.Pedido;
import com.example.apppanaderia.Objetos.Pedidos;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class verPedidos extends AppCompatActivity {
    private RecyclerView recycler;
    private AdapterPedidos adapedidos;
    ImageView imagenVacio;
    TextView textoVacio;
    FrameLayout fmProductos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver_pedidos);
        setUI();
    }

    private void setUI() {
        recycler = findViewById(R.id.RecyclerPedidos);
        recycler.setLayoutManager(new LinearLayoutManager(this));

        adapedidos = new AdapterPedidos(obtenerPedidos(),getApplicationContext());
        recycler.setAdapter(adapedidos);

    }


    public ArrayList<Pedidos> obtenerPedidos(){
        ArrayList<Pedidos> listapedidos = new ArrayList<>();

        listapedidos.add(new Pedidos(1,666666666,"2022-5-5","2022-6-6","pepe",1,false));
        listapedidos.add(new Pedidos(2,777777777,"2022-5-5","2022-6-6","pepe",1,true));
        listapedidos.add(new Pedidos(1,888888888,"2022-5-5","2022-6-6","pepe",1,false));
        listapedidos.add(new Pedidos(3,888888888,"2022-5-5","2022-6-6","pepe",1,true));

//        PedidosDAO pedDAO;
//        try{
//            pedDAO = new PedidosDAO();
//            listapedidos = new ArrayList<>();
//            listapedidos.addAll(Collections.singleton(pedDAO.findByPK(Login.usuario.getID())));
//            return listapedidos;
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }


        return listapedidos;
    }
}