package com.example.apppanaderia.Fragments;

import android.graphics.Color;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.apppanaderia.Adapters.AdapterTodos;
import com.example.apppanaderia.Objetos.Productos;
import com.example.apppanaderia.R;

import java.util.ArrayList;

public class Salado extends Fragment {

    RecyclerView recyclerpro;
    ArrayList<Productos> productoslista;
    ImageView imagenVacio;
    TextView textoVacio;
    FrameLayout fmProductos;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.fragment_lista_productos, container, false);

        imagenVacio = vista.findViewById(R.id.imagenVacio);
        textoVacio = vista.findViewById(R.id.textoVacio);
        fmProductos = vista.findViewById(R.id.FrameLayoutProductos);

        productoslista = new ArrayList<>();
        recyclerpro = vista.findViewById(R.id.RecyclerProductos);
        recyclerpro.setLayoutManager(new LinearLayoutManager(getContext()));

        llenarlista();

        AdapterTodos adapater;

        if (productoslista.isEmpty()){
            imagenVacio.setVisibility(View.VISIBLE);
            textoVacio.setVisibility(View.VISIBLE);
            fmProductos.setBackgroundColor(Color.GRAY);

        }else{
            adapater = new AdapterTodos(productoslista,getContext());
            recyclerpro.setAdapter(adapater);
        }

        return vista;
    }

    private void llenarlista() {

        productoslista.add(new Productos(1,"Coca de harina",2,"salado","coca_harina"));
        productoslista.add(new Productos(2,"Coca de pizza",2.5f,"salado","coca_pizza"));

//        try{
//            ProductosDAO proDAO = new ProductosDAO();
//            Productos productos = new Productos();
//            productos.setCategoria("salado");
//            List<Productos> listaProductos = proDAO.findByExample(productos);
//            productoslista.addAll(listaProductos);
//
//        }catch (Exception e){
//            System.out.println(e.getMessage());
//        }
    }
}