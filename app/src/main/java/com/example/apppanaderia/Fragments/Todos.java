package com.example.apppanaderia.Fragments;

import android.graphics.Color;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.apppanaderia.Adapters.AdapterTodos;
import com.example.apppanaderia.Adapters.VPAdapter;
import com.example.apppanaderia.DAO.ConexionBD;
import com.example.apppanaderia.DAO.ProductosDAO;
import com.example.apppanaderia.Objetos.Productos;
import com.example.apppanaderia.R;
import com.google.android.material.tabs.TabLayout;

import java.sql.Array;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class Todos extends Fragment {

    RecyclerView recyclerpro;
    ArrayList<Productos> productoslista;
    ImageView imagenVacio;
    TextView textoVacio;
    FrameLayout fmProductos;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.fragment_lista_productos, container, false);

        imagenVacio = vista.findViewById(R.id.imagenVacio);
        textoVacio = vista.findViewById(R.id.textoVacio);
        fmProductos = vista.findViewById(R.id.FrameLayoutProductos);

        productoslista = new ArrayList<>();
        recyclerpro = vista.findViewById(R.id.RecyclerProductos);
        recyclerpro.setLayoutManager(new LinearLayoutManager(getContext()));

        llenarlista();

        AdapterTodos adapater;

        if (productoslista.isEmpty()){
            imagenVacio.setVisibility(View.VISIBLE);
            textoVacio.setVisibility(View.VISIBLE);
            fmProductos.setBackgroundColor(Color.GRAY);

        }else{
            adapater = new AdapterTodos(productoslista,getContext());
            recyclerpro.setAdapter(adapater);
        }

        return vista;
    }

    private void llenarlista() {
        productoslista.add(new Productos(1,"Barra casera",1,"panes","barra_casera"));
        productoslista.add(new Productos(2,"Pan de cereales",1.5f,"panes","pan_cereales"));
        productoslista.add(new Productos(3,"Coca de harina",2,"salado","coca_harina"));
        productoslista.add(new Productos(4,"Coca de pizza",2.5f,"salado","coca_pizza"));
        productoslista.add(new Productos(5,"Magdalenas bombon",3,"magdalenas","magdalenas_bombon"));
        productoslista.add(new Productos(6,"Magdalenas caseras",3.5f,"magdalenas","magdalenas_casera"));
        productoslista.add(new Productos(7,"Mantecado de almendra",4,"reposteria","mantecado_almendra"));
        productoslista.add(new Productos(8,"Mantecado",4.5f,"reposteria","mantecados"));
        productoslista.add(new Productos(9,"Rollo",5,"tonyas","rollos"));
        productoslista.add(new Productos(10,"Toña",5.5f,"tonyas","tonya"));
        productoslista.add(new Productos(11,"Sequillos en bolsa",6,"embolsado","sequillos_bolsa"));
        productoslista.add(new Productos(12,"Sequillos sin azucar en bolsa",6.5f,"embolsado","sequillos_sin_azucar_bolsa"));


//        ProductosDAO proDAO;
//        try {
//            proDAO = new ProductosDAO();
//            List<Productos> productos = proDAO.findAll();
//            productoslista.addAll(productos);
//
//        }catch (Exception e){
//            System.out.println(e.getMessage());
//        }


    }

}