package com.example.apppanaderia.Fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.apppanaderia.Adapters.AdapterPedidos;
import com.example.apppanaderia.Adapters.AdapterTodos;
import com.example.apppanaderia.DAO.PedidosDAO;
import com.example.apppanaderia.Login;
import com.example.apppanaderia.Objetos.Pedidos;
import com.example.apppanaderia.Objetos.Productos;
import com.example.apppanaderia.R;

import java.util.ArrayList;
import java.util.List;

public class Pedido extends Fragment {
    RecyclerView recyclerpro;
    ArrayList<Pedidos> pedidoslista;
    ArrayList<Productos> productoslista;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//        View vista = inflater.inflate(R.layout.fragment_lista_pedidos, container, false);
        View vista = inflater.inflate(R.layout.fragment_lista_productos, container, false);

//        pedidoslista = new ArrayList<>();
//        recyclerpro = vista.findViewById(R.id.RecyclerPedidos);

        productoslista = new ArrayList<>();
        recyclerpro = vista.findViewById(R.id.RecyclerProductos);
        recyclerpro.setLayoutManager(new LinearLayoutManager(getContext()));

        llenarlista();

//        AdapterPedidos adapater = new AdapterPedidos(pedidoslista,getContext());
        AdapterTodos adapater = new AdapterTodos(productoslista,getContext());
        recyclerpro.setAdapter(adapater);

        return vista;
    }

    private void llenarlista() {
//        pedidoslista.add(new Pedidos(1,666666666,"2022-5-5","2022-6-6","pepe",1,false));
//        pedidoslista.add(new Pedidos(2,777777777,"2022-5-5","2022-6-6","pepe",1,true));
//        pedidoslista.add(new Pedidos(1,888888888,"2022-5-5","2022-6-6","pepe",1,false));
//        pedidoslista.add(new Pedidos(3,888888888,"2022-5-5","2022-6-6","pepe",1,true));

//        try{
//            PedidosDAO pedDAO = new PedidosDAO();
//            Pedidos pedidos = new Pedidos();
//            pedidos.setIDusuario(Login.usuario.getID());
//            List<Pedidos> listapedido = pedDAO.findByExample(pedidos);
//            pedidoslista.addAll(listapedido);
//
//        }catch (Exception e){
//
//        }

        productoslista.add(new Productos(1,"prueba1",1,"embolsado","coca_pizza"));
        productoslista.add(new Productos(2,"prueba2",1,"panes","coca_harina"));
        productoslista.add(new Productos(3,"prueba3",1,"tonyas","tonya"));
        productoslista.add(new Productos(4,"prueba1",1,"embolsado","sequillos_bolsa"));
        productoslista.add(new Productos(5,"prueba2",1,"panes","pan_cereales"));
        productoslista.add(new Productos(6,"prueba3",1,"tonyas","mantecado_almendra"));
    }
}