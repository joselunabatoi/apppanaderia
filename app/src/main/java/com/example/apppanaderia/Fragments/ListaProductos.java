package com.example.apppanaderia.Fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.apppanaderia.Adapters.AdapterTodos;
import com.example.apppanaderia.Objetos.Productos;
import com.example.apppanaderia.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ListaProductos#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ListaProductos extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    RecyclerView recyclerpro;
    ArrayList<Productos> productoslista;

    public ListaProductos() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ListaProductos.
     */
    // TODO: Rename and change types and number of parameters
    public static ListaProductos newInstance(String param1, String param2) {
        ListaProductos fragment = new ListaProductos();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.fragment_lista_productos, container, false);

        productoslista = new ArrayList<>();
        recyclerpro = vista.findViewById(R.id.RecyclerProductos);
        recyclerpro.setLayoutManager(new LinearLayoutManager(getContext()));

        llenarlista();

        AdapterTodos adapater = new AdapterTodos(productoslista,getContext());
        recyclerpro.setAdapter(adapater);

        return vista;
    }

    private void llenarlista() {
        productoslista.add(new Productos(1,"prueba1",1,"embolsado","sequillos_bolsa"));
        productoslista.add(new Productos(2,"prueba2",1,"panes","pan_cereales"));
        productoslista.add(new Productos(3,"prueba3",1,"tonyas","tonya"));





    }
}