package com.example.apppanaderia;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.apppanaderia.DAO.UsuariosDAO;
import com.example.apppanaderia.Objetos.Usuarios;

import org.json.JSONException;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Login extends AppCompatActivity {
    public static Usuarios usuario;
    private EditText etNombre, etContrasenya;
    private ExecutorService executor;
    public static ArrayList<Usuarios>  usuariosPermitidos = new ArrayList<>();;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setUI();
    }

    private void setUI() {
        Button btRegistrarse, btLogin;

        btRegistrarse = findViewById(R.id.btnRegister);
        btLogin = findViewById(R.id.btnLogin);
        etNombre = findViewById(R.id.etNombre);
        etContrasenya = findViewById(R.id.etContraseña);

        btRegistrarse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Registrarse.class);
                startActivity(intent);
//                Intent intent = new Intent(getApplicationContext(),MenuHorizontal.class);
//                startActivity(intent);
            }
        });

        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (isNetworkAvailable()){

                    if (!etNombre.getText().toString().equals("")){
                        if (!etContrasenya.getText().toString().equals("")){

                            if (usuariosPermitidos.isEmpty()){
                                Toast.makeText(getApplicationContext(),"Debe registrarse",Toast.LENGTH_LONG).show();
                            }else{
                                for (int i = 0; i < usuariosPermitidos.size(); i++) {
                                    if (etNombre.getText().toString().equals(usuariosPermitidos.get(i).getNombre()) && etContrasenya.getText().toString().equals(usuariosPermitidos.get(i).getContraseña())){
                                        Intent intent = new Intent(getApplicationContext(),MenuHorizontal.class);
                                        startActivity(intent);
                                        Toast.makeText(getApplicationContext(),"Login realizado correctamente",Toast.LENGTH_LONG).show();
                                        usuario.setNombre(etNombre.getText().toString());
                                        usuario.setContraseña(etContrasenya.getText().toString());
                                    }
                                }
                            }


                        }else{
                            Toast.makeText(getApplicationContext(),"Porfavor introduzca la contraseña",Toast.LENGTH_LONG).show();
                        }
                    }else{
                        Toast.makeText(getApplicationContext(),"Porfavor Introduzca el nombre",Toast.LENGTH_LONG).show();
                    }











//                    startBackgroundTask();

//                    List<Usuarios> listaUsuarios = null;
//                    UsuariosDAO userDao;
//                    Usuarios user = new Usuarios();
//                    try{
//                        userDao = new UsuariosDAO();
//                        user.setContraseña(etContrasenya.getText().toString());
//                        user.setNombre(etNombre.getText().toString());
//                        listaUsuarios = userDao.findByExample(user);
//                        if (listaUsuarios.size() > 0 ){
//                            usuario = listaUsuarios.get(0);
//                    Intent intent = new Intent(getApplicationContext(),MenuHorizontal.class);
//                    startActivity(intent);

//                        }else{
//                            Toast.makeText(getApplicationContext(),"Porfavor introduzca los credenciales",Toast.LENGTH_LONG).show();
//                        }
//
//                    }catch (Exception e){
//                        System.out.println(e.getMessage());
//                    }


                }
//            }
        });


    }

    private void comprobarUsuario(Usuarios user){

    }


    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Network nw = connectivityManager.getActiveNetwork();
            if (nw == null) return false;

            NetworkCapabilities actNw = connectivityManager.getNetworkCapabilities(nw);

            return actNw != null && (actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) || actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR));


        } else {
            NetworkInfo nwInfo = connectivityManager.getActiveNetworkInfo();
            return nwInfo != null && nwInfo.isConnected();

        }
    }

    private void startBackgroundTask() {
        final int CONNECTION_TIMEOUT = 10000;
        final int READ_TIMEOUT = 7000;

        executor = Executors.newSingleThreadExecutor();
        Handler handler = new Handler(Looper.getMainLooper());

        executor.execute(new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),"Conecta",Toast.LENGTH_LONG).show();
                    }
                });
                List<Usuarios> listaUsuarios = null;
                UsuariosDAO userDao;
                Usuarios user = new Usuarios();
                try{
                    userDao = new UsuariosDAO();
                    Log.d("Debug","Conectat");
                    user.setContraseña(etContrasenya.getText().toString());
                    user.setNombre(etNombre.getText().toString());
                    listaUsuarios = userDao.findByExample(user);
                    Log.d("Debug",listaUsuarios.toString());

                }catch (Exception e){
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),"Error: "+e.getMessage(),Toast.LENGTH_LONG).show();
                        }
                    });
                }


                List<Usuarios> finalListaUsuarios = listaUsuarios;
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),"LISTA: "+finalListaUsuarios.toString(),Toast.LENGTH_LONG).show();
                        if (finalListaUsuarios.size() > 0 ){
                            usuario = finalListaUsuarios.get(0);
                            Intent intent = new Intent(getApplicationContext(),MenuHorizontal.class);
                            startActivity(intent);
                        }else{
                            Toast.makeText(getApplicationContext(),"Porfavor introduzca los credenciales",Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        });
    }

}